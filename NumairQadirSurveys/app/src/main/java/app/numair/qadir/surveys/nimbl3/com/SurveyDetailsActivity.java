package app.numair.qadir.surveys.nimbl3.com;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import app.numair.qadir.surveys.nimbl3.com.utilities.Common;

/**
 * Created by Numair on 8/15/2016.
 */
public class SurveyDetailsActivity extends Activity {

    private ImageView imgFullScreen;
    private String imgUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_details);

        imgFullScreen = (ImageView) findViewById(R.id.imgFullScreen);

        imgUrl = getIntent().getStringExtra("image_url");

        if (!imgUrl.isEmpty())
            Picasso.with(this)
                    .load(imgUrl + Common.Large_Image)
                    .placeholder(R.drawable.logo)
                    .error(R.drawable.logo)
                    .into(imgFullScreen);
        else
            finish();

    }
}
