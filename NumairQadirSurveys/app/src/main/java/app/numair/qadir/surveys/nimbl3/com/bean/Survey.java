package app.numair.qadir.surveys.nimbl3.com.bean;

/**
 * Created by Numair on 8/14/2016.
 */
public class Survey {

    private String title = "";
    private String description = "";
    private String cover_image_url = "";

    public Survey(){
    }

    public Survey(String title, String description, String cover_image_url) {
        this.title = title;
        this.description = description;
        this.cover_image_url = cover_image_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCover_image_url() {
        return cover_image_url;
    }

    public void setCover_image_url(String cover_image_url) {
        this.cover_image_url = cover_image_url;
    }

    @Override
    public String toString() {
        return "Survey{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", cover_image_url='" + cover_image_url + '\'' +
                '}';
    }
}