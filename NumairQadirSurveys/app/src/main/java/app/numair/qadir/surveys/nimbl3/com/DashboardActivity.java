package app.numair.qadir.surveys.nimbl3.com;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.github.pwittchen.reactivenetwork.library.Connectivity;
import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork;

import java.util.ArrayList;
import java.util.List;

import app.numair.qadir.surveys.nimbl3.com.adapter.FragmentSurveyAdapter;
import app.numair.qadir.surveys.nimbl3.com.bean.Survey;
import app.numair.qadir.surveys.nimbl3.com.network.ApiService;
import app.numair.qadir.surveys.nimbl3.com.network.RetroClient;
import app.numair.qadir.surveys.nimbl3.com.utilities.AppLog;
import app.numair.qadir.surveys.nimbl3.com.utilities.AppToast;
import app.numair.qadir.surveys.nimbl3.com.utilities.Common;
import app.numair.qadir.surveys.nimbl3.com.utilities.ui.DirectionalViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Numair on 8/13/2016.
 */
public class DashboardActivity extends FragmentActivity implements DirectionalViewPager.OnPageChangeListener {

    private List<Survey> surveyList;
    private ProgressDialog progressDialog;

    //Set up the pager
    private DirectionalViewPager mDirectionalViewPager;
    private int mSize;
    private int mCurrentItem;
    private ImageView mBg;

    private final static int DIRECTION = DirectionalViewPager.VERTICAL;
    private final static float SCALE = 1.2f;
    private final static float OVER_PERCENTAGE = 3;

    private List<ImageView> imageViewIndicatorList = null;
    private LinearLayout linearLayoutIndicator;
    private int surveyListSize = 0;

    private Button btnReload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        surveyList = new ArrayList<>();
        btnReload = (Button) findViewById(R.id.buttonReload);
        mDirectionalViewPager = (DirectionalViewPager) findViewById(R.id.pager);
        linearLayoutIndicator = (LinearLayout) findViewById(R.id.indicator_ll);
        mBg = (ImageView) findViewById(R.id.mainBgImage);
        mBg.setScaleX(SCALE);
        mBg.setScaleY(SCALE);

        mSize = (int) (Common.getScreenHeight(this) * OVER_PERCENTAGE / 100);
        mCurrentItem = 0;

        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                populateSurveyInternetCheck();
            }
        });

        populateSurveyInternetCheck();
    }

    private void populateSurveyInternetCheck() {
        ReactiveNetwork.observeNetworkConnectivity(getApplicationContext())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Connectivity>() {
                    @Override
                    public void call(Connectivity connectivity) {
                        if (connectivity.getState() == NetworkInfo.State.CONNECTED) {
                            populateSurveyList();
                        } else {
                            if (progressDialog != null && progressDialog.isShowing())
                                progressDialog.dismiss();
                            AppToast.Show(getApplicationContext(), getString(R.string.error_network_exception));
                        }
                    }
                });
    }

    private void populateSurveyList() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.msg_populate));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();

        //Creating an object of our api interface
        ApiService api = RetroClient.getApiService();

        /**
         * Calling JSON
         */
        Call<List<Survey>> call = api.getLatestSurveyList();

        /**
         * Enqueue Callback will be call when get response...
         */
        call.enqueue(new Callback<List<Survey>>() {
            @Override
            public void onResponse(Call<List<Survey>> call, Response<List<Survey>> response) {
                //Dismiss dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */
                    AppLog.Error("Response is successful");
                    AppLog.Error("Response: " + response.body());


                    surveyList = response.body();
                    surveyListSize = surveyList.size();

                    /**
                     * Binding that List to Adapter
                     */

                    mDirectionalViewPager.setAdapter(new FragmentSurveyAdapter(getSupportFragmentManager(), surveyList));
                    mDirectionalViewPager.setOrientation(DIRECTION);
                    mDirectionalViewPager.setOnPageChangeListener(DashboardActivity.this);

                    /**
                     * Populate Page Indicator List acc to list size
                     */
                    initIndicator(surveyListSize);

                } else {
                    AppToast.Show(DashboardActivity.this, "" + getString(R.string.error_parse));
                }
            }

            @Override
            public void onFailure(Call<List<Survey>> call, Throwable t) {
                AppLog.Error("Response is failure");

                // Dismiss progress dialog
                progressDialog.dismiss();
                AppLog.Error("" + t.getMessage());

                // Show response message
                AppToast.Show(DashboardActivity.this, "" + t.getMessage());
            }
        });
    }

    /**
     * Populate page indicator method
     *
     * @param listSize Provide survey list size
     */
    private void initIndicator(int listSize) {
        imageViewIndicatorList = new ArrayList<>();

        linearLayoutIndicator.setOrientation(DIRECTION);
        linearLayoutIndicator.setPadding(Common.dp2px(this, 10), 0, 0, 0);
        linearLayoutIndicator.setGravity(Gravity.CENTER);

        // Generating Image View of Circle Indicator programmatically
        for (int i = 0; i < listSize; i++) {
            ImageView iv = new ImageView(this);
            iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            iv.setPadding(0, Common.dp2px(this, 10), 0, 0);

            if (i == 0) {
                iv.setImageResource(R.drawable.shape_sel);
            } else {
                iv.setImageResource(R.drawable.shape_nor);
            }

            // Adding Views
            imageViewIndicatorList.add(iv);
            linearLayoutIndicator.addView(iv);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == ViewPager.SCROLL_STATE_IDLE) {
            mBg.setY(-mCurrentItem * mSize);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset,
                               int positionOffsetPixels) {
        if (positionOffset == 0.0f)
            return;
        mBg.setY(-((position + positionOffset) * mSize));
    }

    @Override
    public void onPageSelected(int position) {
        mCurrentItem = position;

        for (int i = 0; i < imageViewIndicatorList.size(); i++) {
            if (position == i) {
                imageViewIndicatorList.get(i).setImageResource(
                        R.drawable.shape_sel);
            } else {
                imageViewIndicatorList.get(i).setImageResource(
                        R.drawable.shape_nor);
            }
        }
    }
}
